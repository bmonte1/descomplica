import Header from "./components/header";
import ListarTarefa from "./pages/tarefa/ListarTarefa";

function App() {
  return (
    <div className="App">
      <Header />

      <section style={{ margin: '1.5rem' }}>
        <ListarTarefa />
      </section>
    </div>
  );
}

export default App;
